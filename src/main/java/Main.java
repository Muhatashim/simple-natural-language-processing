import org.muhatashim.nlp.Evaluator;
import org.muhatashim.nlp.Intent;
import org.muhatashim.nlp.OperationIntent;
import org.muhatashim.nlp.ParamIntent;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 2/6/2018
 * Time: 8:28 PM
 */
public class Main {
	private static Logger LOG = Logger.getGlobal();

	public static void main(String[] args) {
		LOG.setLevel(Level.ALL);

		HashMap<String, Intent> params = new HashMap<>();
		params.put("employeeName", new ParamIntent("employee(\\s(named|called))? (.+?)", 3));
		params.put("employeeName2", new ParamIntent("and(\\s(named|called))? (.+?)", 3));
		params.put("location", new ParamIntent("location(\\s(named|called))? (.+?)", 3));
		params.put("location2", new ParamIntent("location(\\s(named|called))? (.+?)", 3));

		OperationIntent insideIntent = new OperationIntent<>(
				"( inside \\$(location) and inside \\$(location2))",
				map -> map.get("location2") + ", " + map.get("location"));
		params.put("inside", insideIntent);
		OperationIntent finalOperationIntent = new OperationIntent<>(
				"(search|find)(\\sfor)?(\\s(an|a))? \\$(employeeName) \\$(employeeName2)\\$(inside)?",
				map -> "Found: " + map.get("employeeName") + " and " + map.get("employeeName2") + " inside "
						+ map.get("inside"));

		Evaluator evaluator = new Evaluator(params, finalOperationIntent);
		LOG.info(evaluator.evaluate(
				"Search for an employee named Muhatashim Zarif and Ayy Lmao inside location called USA and inside "
						+ "location named GA").toString());
		LOG.info(evaluator.evaluate("Find a employee called Sponge The Bob SquarePants and Rick Roll").toString());
		LOG.info(evaluator.evaluate("Find employee Sponge The Bob SquarePants and Rick Roll").toString());
	}
}
