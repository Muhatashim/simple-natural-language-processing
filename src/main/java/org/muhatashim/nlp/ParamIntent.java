package org.muhatashim.nlp;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 2/6/2018
 * Time: 9:01 PM
 */
public class ParamIntent extends Intent {

    private int[] returnGroups;

    public ParamIntent(String regex, int... returnGroups) {
        super(regex);
        this.returnGroups = returnGroups;
    }

    public int[] getReturnGroup() {
        return returnGroups;
    }
}
