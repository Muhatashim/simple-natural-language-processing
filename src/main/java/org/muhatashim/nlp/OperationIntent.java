package org.muhatashim.nlp;

import java.util.HashMap;
import java.util.function.Function;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 2/6/2018
 * Time: 9:12 PM
 */
public class OperationIntent<R> extends Intent {
    private Function<HashMap<String, Object>, R> operation;
    
    public OperationIntent(String operationRegex, Function<HashMap<String, Object>, R> operation) {
        super(operationRegex);
        this.operation = operation;
    }

    public void setOperation(Function<HashMap<String, Object>, R> operation) {
        this.operation = operation;
    }

    public Function<HashMap<String, Object>, R> getOperation() {
        return operation;
    }
}
