package org.muhatashim.nlp;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 2/6/2018
 * Time: 9:26 PM
 */
public class Evaluator {
	private Logger LOG = Logger.getGlobal();
	private static final Pattern parametersPattern = Pattern.compile("\\\\\\$\\((.+?)\\)");

	private HashMap<String, Intent> args;
	private final OperationIntent operationIntent;

	public Evaluator(HashMap<String, Intent> args, OperationIntent operationIntent) {
		this.args = args;
		this.operationIntent = operationIntent;
	}

	public Object evaluate(String input) {
		return operationIntent.getOperation().apply(extract(input));
	}

	public HashMap<String, Object> extract(String input) {
		AbstractMap.SimpleEntry<String, List<ParamIndexMatch>> flattenedOperation =
				flattenIntentRegex(operationIntent.getIntentRegex());
		String operationRegex = flattenedOperation.getKey();
		List<ParamIndexMatch> paramIndexes = flattenedOperation.getValue();
		LOG.info("Using pattern: " + operationRegex);

		Pattern operationPattern = Pattern.compile("^" + operationRegex + "$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = operationPattern.matcher(input);
		HashMap<String, Object> paramValues = new HashMap<>();
		while (matcher.find()) {
			for (int i = 0; i < paramIndexes.size(); i++) {
				ParamIndexMatch paramIndexMatch = paramIndexes.get(i);

				if (paramIndexMatch.getIntent() instanceof OperationIntent) {
					OperationIntent operationIntent = (OperationIntent) paramIndexMatch.getIntent();
					String subEvalString = matcher.group(paramIndexMatch.getIndex());
					if (subEvalString != null) {
						Evaluator subEvaluator = new Evaluator(args, operationIntent);
						Object eval = operationIntent.getOperation().apply(subEvaluator.extract(subEvalString));
						paramValues.put(paramIndexMatch.getVariableName(), eval);
					}
				}

				if (paramIndexMatch.getIntent() instanceof ParamIntent) {
					ParamIntent paramIntent = (ParamIntent) paramIndexMatch.getIntent();
					int[] returnGroupIndexes = paramIntent.getReturnGroup();
					String[] returnGroups = new String[returnGroupIndexes.length];
					for (int i2 = 0; i2 < returnGroups.length; i2++) {
						returnGroups[i2] = matcher.group(paramIndexMatch.getIndex() + returnGroupIndexes[i2]);
					}
					paramValues.put(paramIndexMatch.getVariableName(),
					                returnGroups.length == 1 ? returnGroups[0] : returnGroups);
				}
			}
		}

		return paramValues;
	}

	protected AbstractMap.SimpleEntry<String, List<ParamIndexMatch>> flattenIntentRegex(String operationRegex) {
		Matcher paramsMatcher = parametersPattern.matcher(operationRegex);
		String noParamRegex = paramsMatcher.replaceAll("");
		StringBuffer replacedParams = new StringBuffer();

		int groupCounter = getGroupCount(noParamRegex);
		List<ParamIndexMatch> paramGroupStartIndex = new ArrayList<>();
		paramsMatcher.reset();
		while (paramsMatcher.find()) {
			String group = paramsMatcher.group(1);
			Intent intent = args.get(group);
			String paramReplacement = "";

			if (intent == null) {
				throw new IllegalArgumentException(group + " must be defined");
			}
			if (intent instanceof OperationIntent) {
				AbstractMap.SimpleEntry<String, List<ParamIndexMatch>> childOperation =
						flattenIntentRegex(intent.getIntentRegex());
				paramReplacement += childOperation.getKey();
				for (ParamIndexMatch paramIndexMatch : childOperation.getValue()) {
					paramIndexMatch.setIndex(groupCounter + paramIndexMatch.getIndex());
					paramGroupStartIndex.add(paramIndexMatch);
				}
				paramGroupStartIndex.add(new ParamIndexMatch(groupCounter + 1, group, intent));
			}
			if (intent instanceof ParamIntent) {
				paramReplacement += intent.getIntentRegex();
				paramGroupStartIndex.add(new ParamIndexMatch(groupCounter, group, intent));
			}

			groupCounter += getGroupCount(paramReplacement);
			paramsMatcher.appendReplacement(replacedParams, paramReplacement.replace("\\", "\\\\"));
		}
		paramsMatcher.appendTail(replacedParams);
		return new AbstractMap.SimpleEntry<>(replacedParams.toString(), paramGroupStartIndex);
	}

	protected int getGroupCount(String regex) {
		return Pattern.compile(regex).matcher("").groupCount();
	}

	private static class ParamIndexMatch {
		private int index;
		private String variableName;
		private Intent intent;

		public ParamIndexMatch(int index, String variableName, Intent intent) {
			this.index = index;
			this.variableName = variableName;
			this.intent = intent;
		}

		public int getIndex() {
			return index;
		}

		public String getVariableName() {
			return variableName;
		}

		public Intent getIntent() {
			return intent;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public void setVariableName(String variableName) {
			this.variableName = variableName;
		}

		public void setIntent(Intent intent) {
			this.intent = intent;
		}
	}
}
